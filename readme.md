## Solutis - Talent Sprint 2020 - Coding a robot!

RoboCode feito em Java!
 
RobotDias - a robot by Victor Dias Dos Santos Assis

Email - victor.assis@ucsal.edu.br

### Objetivo do Código

<p style="text-align: justify;"> O RobotDias é um robô estratégico, que possui dois tipos de movimentações para situações de batalha diferentes. Quando no início do combate forem detectados mais de um robô inimigo no campo, o RobotDias tomará a primeira decisão de avançar até uma das bordas do campo e manter-se percorrendo todas as bordas do campo atirando em inimigos usando sua força mínima de disparo, ou seja, a estratégia no início da batalha é a sobrevivência e sustentabilidade. Todo processo em questão se repete até que reste somente um robô inimigo vivo no campo, quando o RobotDias decidirá ser mais agressivo e rápido, decidindo então perseguir o inimigo restante no campo, efetuando disparos de máxima contundência contra o robô inimigo.</p>     



### Pontos Fortes da implementação:

* A mudança de movimentação do RobotDias agrega como um recurso surpresa que acaba surpreendendo robôs que chegam até o fim do combate usando estratégias mais passivas

* A movimentação pelas laterais da arena mantém o RobotDias fora do trajeto de grande parte dos disparos efetuados por outros robôs.

* A  perseguição é feita através da detecção de um robô inimigo em um raio de 360º, sendo uma estratégia muito forte no “um contra um”.

### Pontos Fracos da implementação:

* As colisões com outros robôs podem acabar atrapalhando a estratégia inicial.

* A perseguição é menos eficaz contra robôs que se movimentam circularmente, pois, as curvas do RobotDias são realizadas de acordo a detecção do grau de curvatura entre um robô e outro no exato momento da detecção feita pelo radar, fazendo com que a margem de erro dos disparos efetuados seja maior contra alvos com muitos movimentos laterais ou circulares.  

### Experiência

<p style="text-align: justify;">O robocode me proporcionou o primeiro contato com Thread’s em jogos e a experiência de poder estar testando minhas mudanças no código com outros robôs foi incrível. A biblioteca do robocode possui uma rede de informações muito intuitiva e métodos com funções que provocam a sua criatividade a todo momento. Achei a proposta da Solutis diferente e muito interessante!</p>
Obrigado pela oportunidade!    
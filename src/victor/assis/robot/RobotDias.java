package victor.assis.robot;

import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * RobotDias - a robot by (Victor Dias Dos Santos Assis)
 */
public class RobotDias extends AdvancedRobot {
	double borda;
	// true se for "um contra um"
	boolean oneVone;

	/**
	 * run: RobotDias's default behavior
	 */
	public void run() {

		setColors(Color.red, Color.white, Color.red); // body,gun,radar

		// capturando as bordas do campo de batalha
		borda = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
		
		// Se mais de um robo inimigo estiver registrado e vivo no campo de batalha
		if (getOthers() > 1) {
			oneVone = false;
			turnLeft(getHeading() % 90);
			// O robotDias avan�a at� a borda
			ahead(borda);
			// o robotDias se posiciona de forma lateral com a arma apontada para o campo
			turnGunRight(90);
			turnRight(90);
		}else {
			oneVone = true;
		}
		// Loop principal
		while (true) {
			// Se mais de um robo inimigo estiver registrado e vivo no campo de batalha
			if (getOthers() > 1) {
				// Repita o processo de percorrer as bordas do campo
				ahead(borda);
				turnRight(90);
				// Se somente um robo inimigo estiver registrado e vivo no campo de batalha e o
				// estado anterior da batalha tiver sido composto por mais de um robo inimigo
			} else if (getOthers() == 1 && oneVone == false) {
				// A batalha agora � um contra um
				oneVone = true;
				stop();
				// A estrat�gia de contornar o campo n�o � mais t�o eficiente
				// Para in�cio da pr�xima extrat�gia, a arma � retomada ao seu estado inicial
				turnGunLeft(90);
				// Se somente um robo inimigo estiver registrado e vivo no campo de batalha e o
				// estado anterior de batalha for "um contra um"
			} else if (getOthers() == 1 && oneVone == true) {
				// O robotDias agora caminha de 200 em 200 pixels de forma cont�nua
				setAhead(200);
				// O radar do robotDias agora captura a movimenta��o do robor inimigo em um raio
				// de a��o de 360
				setTurnRadarRight(360);
				execute();
			}

		}

	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {

		// Se mais de um robo inimigo estiver registrado e vivo no campo de batalha
		if (getOthers() > 1) {
			// Dispara com for�a reduzida, pois a economia de muni��o � necess�ria at� o
			// momento de "um contra um"
			setFire(1);
			// A mesma verifica��o que � feita dentro do while no m�todo run, visando
			// acelerar o tempo de mudan�a de estrat�gia
		} else if (getOthers() == 1 && oneVone == false) {
			oneVone = true;
			stop();
			turnGunLeft(90);
			// Se somente um robo inimigo estiver registrado e vivo no campo de batalha
		} else if (getOthers() == 1) {
			// O robotDias faz as curvas de acordo ao posicionamento do robor inimigo
			// identicado pelo m�todo onScannedRobotEvent
			setTurnRight(e.getBearing());
			// Espera a curva acorrer(Talvez desnecess�rio)
			waitFor(new TurnCompleteCondition(this));
			// Momento em que a economia de balas far� a diferen�a e a arma ser� disparada
			// com for�a m�xima
			setFire(Rules.MAX_BULLET_POWER);
		}
	}
}
